import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.page.html',
  styleUrls: ['./alert.page.scss'],
})
export class AlertPage implements OnInit {
  
  titulo: string;
  constructor(public alertCtrl: AlertController) { }
  
  ngOnInit() {
  }
  
  async presentInput(){
    const input = await this.alertCtrl.create({
      header: 'Input',
      subHeader: 'Adicione seu nome: ',
      inputs : [
        {
          name: 'txtNome',
          type: 'text',
          placeholder: 'Nome'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Cancelar');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Ok',data);
            this.titulo = data.txtNome;
          }
        }
      ]
    });
    await input.present();
  }
  
  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Alert Page',
      message: 'Mensagem de teste do Alert.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Cancelar');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Veridico');
          }
        }
      ]
    });
    
    await alert.present();
  }
  
}
