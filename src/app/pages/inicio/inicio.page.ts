import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  
  componentes: Componente[] = [
    {
      icon : 'ellipsis-horizontal-circle',
      name : 'Action Sheet',
      redirectTo : '/action-sheet'
    },
    {
      icon : 'logo-google-playstore',
      name : 'Alert',
      redirectTo : '/alert'
    },
    {
      icon : 'beaker',
      name : 'Avatar',
      redirectTo : '/avatar'
    },
    {
      icon : 'radio-button-off-outline',
      name : 'Botão e Rotas',
      redirectTo : '/botoes'
    },
    {
      icon : 'card',
      name : 'Cards',
      redirectTo : '/card'
    },
    {
      icon : 'checkbox-outline',
      name : 'CheckBox',
      redirectTo : '/check'
    },
    {
      icon : 'calendar-sharp',
      name : 'DateTime',
      redirectTo : '/date-time'
    }    
    
  ];
  
  
  constructor() { }
  
  ngOnInit() {
  }
  
}

interface Componente {
  icon : string,
  name : string,
  redirectTo : string;
}
