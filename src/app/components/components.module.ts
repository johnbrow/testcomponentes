import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from "./header/header.component";
import { IonicModule } from '@ionic/angular';
// PRIMEIRO IMPORTAR E DEPOIS DECLARAR 
// E DEPOIS USAR O MODULO IONIC PARA AS TAGS FUNCIONAREM

@NgModule({
  declarations: [
    HeaderComponent
  ],
  // E DEPOIS EXPORTAR COMPONENTE PARA OUTRAS PAGINAS
  exports: [
    HeaderComponent
  ],

  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
